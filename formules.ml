(* formule du calcul des prédicats *)
type formule =
  | Var of string
  | Neg of formule
  | Et of formule * formule
  | Ou of formule * formule


let rec string_of_formule f =
  match f with
  | Var(s) -> s
  | Neg(f) -> "Neg " ^ (string_of_formule f)
  | Et(f,g) -> "(" ^ string_of_formule f ^ " Et " ^ string_of_formule g ^ ")"
  | Ou(f,g) -> "(" ^ string_of_formule f ^ " Ou " ^ string_of_formule g ^ ")"
;;



let rec insert x l =
  if(List.mem x l)then l else
    match l with
    | [] -> [x]
    | t::q -> if(x<t)then x::t::q else t:: (insert x q);;

let rec union_sorted l1 l2 =
  match l1 with
    [] -> l2
  | t::q -> union_sorted q (insert t l2);; 

(* renvoie la liste des variables apparaissant dans f*)
let rec list_of_vars f =
  match f with
  | Var(s) -> [s]
  | Neg(f) -> list_of_vars f
  | Et(f,g) -> union_sorted (list_of_vars f) (list_of_vars g)
  | Ou(f,g) -> union_sorted (list_of_vars f) (list_of_vars g)
;;

(* evalue f avec l'affectation l (l ensemble de couples (variable,valeur) *)
let rec eval_formule f l =
  match f with
  | Var(s) -> List.assoc s l 
  | Neg(f) -> not (eval_formule f l)
  | Et(f,g) -> (eval_formule f l) && (eval_formule g l) 
  | Ou(f,g) -> (eval_formule f l) || (eval_formule g l)
;; 




let rec desc_neg f =
  match f with
  | Var(s) -> Var(s)
  | Et(f,g) -> Et(desc_neg f,desc_neg g)
  | Ou(f,g) -> Ou(desc_neg f,desc_neg g)
  | Neg(p) -> match p with
    | Var(s) -> Neg(p)
    | Neg(q) -> desc_neg q
    | Et(r,g) -> Ou ( desc_neg (Neg r) , desc_neg (Neg g) )
    | Ou(r,g) -> Et ( desc_neg (Neg r) , desc_neg (Neg g) )
;;

let rec desc_ou f =
  match desc_neg f with
  | Var(s) -> Var(s)
  | Neg(p) -> Neg(desc_ou p)
  | Et(f,g) -> Et(desc_ou f,desc_ou g)
  | Ou(f,g) -> 
      match (desc_ou f,desc_ou g) with 
      | (f,Et(g,h)) -> Et( desc_ou (Ou(f,g)),desc_ou (Ou(f,h)) )
      | (Et(f,g),h) -> Et( desc_ou (Ou(f,h)),desc_ou (Ou(g,h)) ) 
      | (f,g) -> Ou (f,g)
          
;;

(* met f sous fnc *)
let fnc f =
  desc_ou f;;
