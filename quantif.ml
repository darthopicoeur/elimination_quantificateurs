open Printf;;

type quantif = string;;

type terme = 
	| Int of int
	| Var of quantif
	| Plus of terme * terme
	| Produit of int * terme
;;

type operateur = Egal | Inf | Div | NoDiv;;

type relation = Ou | Et;;
type quantifier = PourTout | Existe;;

type formule =
  | Vrai
  | Faux
  | Atomique of operateur * terme * terme
  | Not of formule
  | Relation of relation * formule * formule
  | Quantifier of quantifier * quantif * formule
;;


let quantif_to_string q =
	q
;;
let rec terme_to_string v = 
	match v with 
		| Int(i) -> string_of_int i
		| Var(v) -> quantif_to_string v
		| Plus(a,b) -> (terme_to_string a) ^ "+" ^ (terme_to_string b)
		| Produit(i,b) -> (string_of_int i) ^ "*(" ^ (terme_to_string b) ^ ")"
;;

let rec formule_to_string f =
  match f with
  | Vrai -> "True"
  | Faux -> "False"
  | Atomique(o,a,b) -> "("^(terme_to_string a)^")" ^ (match o with Egal->"="|Inf->"<"|Div->"|"|NoDiv->"┼") ^ "("^(terme_to_string b)^")" 
  | Not(f) -> "Not(" ^ (formule_to_string f) ^ ") "
  | Relation(r,f,g) -> "(" ^ formule_to_string f ^ (match r with Et->"Et"|Ou->"Ou") ^ formule_to_string g ^ ")"
  | Quantifier(q,x,f) -> (match q with PourTout->"Pour Tout"|Existe->"Existe") ^ (quantif_to_string x) ^ " " ^ formule_to_string f ^ ") "
;;

let rec formule_to_string2 f =
	let rec space x =
	 	if(x>0)then "| "^space (x-1) 
	 	else ""
	in

	let rec aux f x =
		space(x) ^ match f with
			| Vrai -> "True"
		    | Faux -> "False"
			| Atomique(o,a,b) -> "("^(terme_to_string a)^")" ^ (match o with Egal->"="|Inf->"<"|Div->"|"|NoDiv->"┼") ^ "("^(terme_to_string b)^")" 
			| Not(f) -> "Not\n" ^ (aux f (x+1))
			| Relation(r,f,g) -> (match r with Et->"Et"|Ou->"Ou") ^ "\n" ^ aux f (x+1) ^ "\n" ^ aux g (x+1)
			| Quantifier(q,v,f) ->  (match q with PourTout->"Pour Tout "|Existe->"Existe ") ^ (quantif_to_string v) ^ "\n" ^ (aux f (x+1))
	in

	aux f 0
;;



(* remplace toutes les variables x par le terme y dans une formule sans quantifieurs*)
let rec replace_terme f x y =
	let rec replace a =
		match a with 
			| Var(_) -> if(a=x)then y else a
			| Int(i) -> Int(i)
			| Plus(a,b) ->  Plus(replace a,replace b)
			| Produit(a,b) -> Produit(a,replace b)
	in
	match f with 
	| Atomique(o,a,b) -> Atomique(o,replace a,replace b)
	| Not(f) -> Not (replace_terme f x y)
	| Relation(r,f,g) -> Relation(r,replace_terme f x y,replace_terme g x y)
	| Quantifier(q,a,f) -> failwith("fail replace_terme")
	| f -> f
;;





(*returne true si t contient une variable*)
let rec contientVar t=
	match t with
	| Int(_) -> false
	| Var(_) -> true
	| Plus(a,b) -> (contientVar a) || (contientVar b)
	| Produit(_,a) -> contientVar a
;;

exception Terme_contient_variable;;
let rec value t = 
	match t with
	| Int(i) -> i
	| Var(_) -> raise Terme_contient_variable
	| Plus(a,b) -> (value a) + (value b)
	| Produit(i,a) -> i*(value a)
;;


let rec valueForm f =
	match f with
	|Vrai -> true
	|Faux -> false
	|Not(f) -> not (valueForm f)
	|Quantifier(_,_,_) -> failwith("fail valueForm")
	|Relation(r,f,g) -> 
		(match r with
			| Et -> valueForm f && valueForm g
			| Ou -> valueForm f || valueForm g
		)
	|Atomique(o,a,b) -> 
		(match o with
			| Egal -> value a = value b
			| Inf -> value a < value b
			| Div -> value b mod value a = 0
			| NoDiv -> not (value b mod value a = 0)
		) 
;;


let rec specifyValues f =
	let rec space x =
	 	if(x>0)then "| "^space (x-1) 
	 	else ""
	in
	let rec aux f x =
		let v = (try (if valueForm f then "  v" else "  f") with |_->"") in
		space(x) ^ (match f with
			| Vrai -> "True"
		    | Faux -> "False"
			| Atomique(o,a,b) -> "("^(terme_to_string a)^")" ^ (match o with Egal->"="|Inf->"<"|Div->"|"|NoDiv->"┼") ^ "("^(terme_to_string b)^")" ^ v
			| Not(f) -> "Not" ^ v ^ "\n" ^ (aux f (x+1))
			| Relation(r,f,g) -> (match r with Et->"Et"|Ou->"Ou") ^ v ^ "\n" ^ aux f (x+1) ^ "\n" ^ aux g (x+1)
			| Quantifier(q,v,f) ->  (match q with PourTout->"Pour Tout "|Existe->"Existe ") ^ (quantif_to_string v) ^ "\n" ^ (aux f (x+1))
		)
	in
	aux f 0
;;

(*
(*transforme not a|b en une disjonctions de divisions (a est un entier) *)
let rec elimDivNegation a b =
	let rec aux i =
		if(i<(a-1))then Relation(Ou, Atomique(Div,Int a,Plus(b,Int i) ), aux (i+1))
		else Atomique(Div,Int a,Plus(b,Int i) )
	in
	aux 1
;;
*)


(*prends une formule sans quantificateur et supprime les négations (step 1 du nouvel algo) *)
let rec removeNeg f =
	match f with
	| Vrai -> f
    | Faux -> f
	| Atomique(o,a,b) -> Atomique(o,a,b)
	| Relation(r,f,g) -> Relation(r,removeNeg f,removeNeg g)
	| Quantifier(_,_,_) -> failwith("fail removeNeg: quantifier")
	| Not(n) -> match n with
		| Vrai -> Faux
    	| Faux -> Vrai
		| Not(q) -> removeNeg q
		| Quantifier(_,_,_) -> failwith("fail removeNeg: quantifier 2")
		| Relation(r,f,g) ->  Relation((match r with Et->Ou |Ou->Et), removeNeg (Not f) , removeNeg (Not g) )
		| Atomique(o,a,b) -> match o with
			| Egal -> Relation(Ou, Atomique(Inf,a,Plus (b,Int 1)), Atomique(Inf,b,Plus(a,Int 1)) )
			| Inf -> Atomique(Inf,b,Plus(a,Int 1) )
			| Div -> Atomique(NoDiv,a,b)(*elimDivNegation (value a) b*)
			| NoDiv -> Atomique(Div,a,b)
;;


(*distribut les * sur les +*)
(*sur les termes*)
let rec _distribProduit t =
	match t with
	|Int(i) -> t
	|Var(v) -> t
	|Plus(a,b) -> Plus(_distribProduit a,_distribProduit b)
	|Produit(a,b) -> 
	match b with
		|Int(_) -> t
		|Var(_) -> t
		|Plus(c,d) -> Plus(_distribProduit (Produit(a,c)), _distribProduit (Produit(a,d)) )
		|Produit(c,d) -> _distribProduit (Produit(a*c,d))
;;
(*sur les formules*)
let rec distribProduit f=
	match f with
	|Vrai -> f
    |Faux -> f
	|Not(a) -> Not(distribProduit a)
	|Relation(r,a,b) -> Relation(r,distribProduit a,distribProduit b)
	|Quantifier(q,x,f) -> Quantifier(q,x,distribProduit f)
	|Atomique(o,a,b) -> Atomique(o,_distribProduit a,_distribProduit b)
;;

(*rend le terme t plus lisible*)
let symplifyTerm t=
	
	let  insert t x n =
		let rec ins_aux t = 
			match t with 
			|Int(_)->(t,false)
			|Var(_)->if t=x then (Produit(n+1,x),true) else (t,false)
			|Plus(a,b)-> let a1,a2 = ins_aux a in (if a2 then (Plus(a1,b),true) else let b1,b2 = ins_aux b in (Plus(a1,b1), a2||b2) )
			|Produit(a,b)->match b with Var(_)->if b=x then (Produit(a+n,b),true) else (t,false) |_-> (t,false)
		in
		let(a,b) = ins_aux t in if b then a else Plus(a, Produit(n,x))
	in

	let rec union t1 t2 =
		match t1 with 
		|Int _ -> failwith("fail symplifyTerm union")
		|Var _ -> insert t2 t1 1
		|Produit(a,b)-> insert t2 b a
		|Plus(a,b)-> union (union a b) t2
	in

	let rec aux t =
		match t with 
		| Int(i) -> (i,None)
		| Var(_) -> (0,Some t)
		| Produit(a,b) ->  (*b devrait être un Int ou un Var*)
			(match b with | Int i -> (a*i,None) | Var _ -> (0,if a=1 then Some b else if a!=0 then Some t else None) | _ -> failwith("fail symplifyTerm") )
		| Plus(a,b) -> let (i,j) = aux a and (k,l) = aux b in 
		(i+k, 	
			(
			match j,l with 
			|None,None -> None 
			|Some t,None |None,Some t -> Some t
			|Some o,Some p ->Some (union o p)
			)
		)
	in

	let (a,b) = aux (_distribProduit t) in 
		match a,b with 
		| i,None -> Int i
		| 0,Some t -> t
		| i,Some t -> Plus(t,Int i) (*les entiers à droite pour plus de claireté*)
;;




let rec symplifyFormule f =
	match distribProduit f with
	|Vrai -> Vrai
    |Faux -> Faux
	|Not(a) -> Not(symplifyFormule a)
	|Relation(r,a,b) -> Relation(r,symplifyFormule a,symplifyFormule b)
	|Quantifier(q,x,f) -> Quantifier(q,x,symplifyFormule f)
	|Atomique(o,a,b) -> Atomique(o,symplifyTerm a,symplifyTerm b)
;;

(*renvoie un couple (multiple de x, a sans les termes dépendant de x)
a ne possède pa de termes de la forme a*(b+c)*)
let rec xTerm a x =
	match a with
	| Int(_) -> (0,a)
	| Var(_) -> if 	a=x then (1,Int 0) else (0,a)
	| Plus(a,b) -> let (k,j) = xTerm a x in let (u,i) = (xTerm b x) in (k+u,Plus(j,i))
	| Produit(i,n) -> if n=x then (i,Int 0) else (0,a)
;;




(*step 3*)
let rec step3 f x =
	match distribProduit f with
	| Vrai -> f
    | Faux -> f
	| Not(f) -> Not(step3 f x)
	| Relation(r,a,b) -> Relation(r,step3 a x,step3 b x)
	| Quantifier(_,_,_) -> failwith("fail step2: quantifier")
	| Atomique(o,a,b) -> 
	match o with
		| Egal -> step3 (Relation(Et, Atomique(Inf,a,Plus(b,Int 1)), 
									 Atomique(Inf,b,Plus(a,Int 1)) )
						) x
		| Inf -> let (c,d) = xTerm a x in let (e,f) = xTerm b x in 
			if c=0 && e=0 then Atomique(o,a,b) else
			if c>e then Atomique(Inf, Produit(c-e,x), Plus(Produit(-1,d),f))
			else Atomique(Inf, Plus(d,Produit(-1,f)), Produit(e-c,x))
		| Div |NoDiv -> f 
;;


let rec pgcd a b =
    if(a=0) then b else pgcd (b mod a) a 
;;

let _lcm a b = 
	a*b/(pgcd a b)
;;

(*lcm de la liste l*)
let lcm l =
	let rec aux l a =
		match l with
		| [] -> a
		| t::q ->  aux q (_lcm a t)
	in
	aux l 1 
;;


(*pour le step 4*)
let lambdaLCM f x = 
	let rec aux f =
		match f with
		| Vrai -> 0
    	| Faux -> 0
		| Not(f) -> aux f
		| Relation(r,f,g) -> (match (aux f,aux g) with |(0,i)|(i,0)->i |(a,b)-> _lcm a b)
		| Quantifier(q,x,f) -> failwith("fail lambdaLCM")
		| Atomique(o,a,b) -> let(k,_) = xTerm a x in let (j,_) = xTerm b x in
			if (k>0 || j>0) then k+j else 0
	in
	aux f
;;


let step4 f x =
	let d = lambdaLCM f x in
	let rec aux f =
		match f with
		| Vrai -> f
    	| Faux -> f
		| Not(f) -> Not(aux f)
		| Relation(r,f,g) -> Relation(r,aux f,aux g)
		| Quantifier(q,x,f) -> failwith("fail step4 quantifier")
		| Atomique(o,a,b) -> 
			match o with 
			| Egal -> failwith("fail step4 egal")
			| Inf -> let (a1,a2) = xTerm a x and (b1,b2) = xTerm b x in
				if a1=0 && b1=0 then (*failwith("fail step4: inf sans x")*)f else
				if a1=0 then Atomique(Inf, Produit(d/b1,Plus(a2,Produit(-1,b2) )), x) else
				if b1=0 then Atomique(Inf, x, Produit(d/a1,Plus(b2,Produit(-1,a2)) )) else 
				if a1>b1 then Atomique(Inf, x, Produit(d/(a1-b1), Plus(b2,Produit(-1,a2))) )
				else Atomique(Inf, Produit(d/(b1-a1), Plus(a2,Produit(-1,b2))), x)
			| Div |NoDiv -> let (b1,b2) = xTerm b x in 
				if b1>0 then Atomique(o,Produit(d/b1,a),Plus(x,Produit(d/b1,b2) )) 
				else f
	in
	if d>0 then Relation(Et,aux f,Atomique(Div,Int d,x) )
	else f
;;



let deltaLCM f x =
	let rec aux f =
		match f with
		| Vrai -> []
    	| Faux -> []
		| Atomique(o,a,b) -> (
			match o with 
			|Div |NoDiv -> let (tmp,_) = xTerm b x in if tmp!=0 then [value a] else []
			| _ -> [])
		| Not(g) -> aux g
		| Relation(r,f,g) -> (aux f) @ (aux g)
		| Quantifier(_,_,_) -> failwith("fail LCM")
	in
	lcm (aux f)
;;

let rec listeBi f x =
	match f with
		| Vrai -> [] 
    	| Faux -> []
		| Not(g) -> listeBi g x
		| Relation(r,f,g) -> listeBi f x @ (listeBi g x)
		| Quantifier(_,_,_) -> failwith("fail listeBi")
		| Atomique(o,a,b) -> 
			match o with 
			|Inf -> let (c,_)=xTerm a x in let (d,_)=xTerm b x in if c=0 && d!=0 then [a] else []
			|_ ->[]
;;



let rec elimTheorem f x lcm bi =

	let rec getMoinsInf f = 
		match f with 
		| Vrai -> f
    	| Faux -> f
		| Atomique(o,a,b) -> (
			match o with 
			|Inf -> let (anb,_) = xTerm a x and (bnb,_) =xTerm b x in if anb=0 && bnb=0 then f else (if anb>bnb then Vrai else Faux)
			|_ ->f)
		| Not(g) ->Not(getMoinsInf g)
		| Relation(r,f,g) -> Relation(r,getMoinsInf f,getMoinsInf g)
		| Quantifier(_,_,_) -> failwith("fail getMoinsInf")
	in

	let fMoinsInf = getMoinsInf f in

	(*créée la discjonction des F moins l'infini*)
	let rec aux j =
		if(j<lcm)then Relation(Ou,replace_terme fMoinsInf x (Int j),aux (j+1) )
		else replace_terme fMoinsInf x (Int j)
	in

	(*créée la disjonction des F(bi+j)*)
	let rec aux2 j bi =
		match bi with
		| [] -> failwith("fail elimTheorem")
		| [a] -> replace_terme f x (Plus(a,Int j))
		| t::q -> Relation(Ou,replace_terme f x (Plus(t,Int j)), aux2 j q)
	in

	let rec aux3 j =
		if(j<lcm)then Relation(Ou,aux2 j bi, aux3 (j+1))
		else aux2 j bi
	in

	match bi with 
 	| [] -> aux 1
	| _ -> if bi!=[] then Relation(Ou, aux 1, aux3 1) else aux 1
;;



let printSteps = ref false;;
(* prends une formule de la forme Existe(x)F (sans autre quantificateur)
et élimine le quantificateur de F *)
let  elimUnQuantif f  =
	let toString = if !printSteps then specifyValues else formule_to_string2 in
	(if !printSteps then printf ("step 0\n%s\n------------\n\n\n\n") (toString f) );
	match f with
	|Quantifier(Existe,x,f)->
		let f2 = removeNeg f in (if !printSteps then printf ("step 2\n%s\n------------\n\n\n\n") (toString (symplifyFormule f2)) );
		let f3 = step3 f2 (Var x) in (if !printSteps then printf ("step 3\n%s\n------------\n\n\n\n") (toString (symplifyFormule f3)) );
		let f4 = step4 f3 (Var x) in (if !printSteps then printf ("step 4\n%s\n------------\n\n\n\n") (toString (symplifyFormule f4)) );
		let g = elimTheorem f4 (Var x) (deltaLCM f4 (Var x)) (listeBi f4 (Var x)) in (if !printSteps then printf ("final: \n%s\n------------\n\n\n\n") (toString (symplifyFormule g)) );
		g
	| _ -> failwith("fail elimQuantif")
;;


(*élimine tous les quantificateurs de f*)
let rec elimQuantif f =
	match f with
	| Vrai -> f
    | Faux -> f
	| Atomique(_,_,_) -> f
	| Not(f) -> Not(elimQuantif f)
	| Relation(r,f,g) -> Relation(r,elimQuantif f,elimQuantif g)
	| Quantifier(q,x,form) ->
		match q with 
			|Existe -> elimUnQuantif(Quantifier(Existe,x,elimQuantif form))
			|PourTout->Not(elimUnQuantif (Quantifier(Existe,x,Not(elimQuantif form))) )
;;




let minFormSize = ref 1;;
let maxFormSize = ref 3;;

let rec gen_terme x i =
	if i<=0 then Int 1 else  
	match Random.int 4 with
	|0->x
	|1->Int( max (Random.int  i) 1)
	|2->let tmp = Random.int i in Plus(gen_terme x (tmp),gen_terme x (i-tmp) )
	|_->let tmp = Random.int i +1 in Produit(tmp ,gen_terme x (i/tmp) )
;;

(*génère une formule aléatoirement*)
let rec gen_rand_form () =
	let gen_quantif l =
		let rec aux_q l s =
			let c = Char.escaped(Char.chr(Random.int (Char.code 'z'-(Char.code 'a')) +(Char.code 'a') )) in
			let s2 = s ^ c in
			if not (List.mem s2 l) then s2 else (aux_q l s2)
		in
		aux_q l "_"
	in
	let get_rand_quantif l =
		List.nth l (Random.int (List.length l))
	in
	let rec aux proba l h = 
	if h>(!maxFormSize) || (Random.float(1.0)>proba && h>(!minFormSize) ) then
		(	
			let x () = if l!=[] then Var (get_rand_quantif l) else gen_terme (Int (Random.int 10)) 10 in
			match Random.int 3 with
			| 0 -> Atomique(Inf ,gen_terme (x()) 10, gen_terme (x()) 10)
			| 1 -> Atomique(Egal,gen_terme (x()) 10, gen_terme (x()) 10)
			| _ -> Atomique(Div ,gen_terme (Int (Random.int 10 +1)) 10, gen_terme (x()) 10)
		)
	else (
		let x = Random.int(5) in
		let proba2 = Random.float(proba) in
		match x with
			| 0 -> Not (aux proba2 l (h+1))
			| 1 -> Relation(Ou,(aux proba2 l (h+1)), (aux proba2 l (h+1)) )
			| 2 -> Relation(Et,(aux proba2 l (h+1)), (aux proba2 l (h+1)) )
			| 3 -> let x = gen_quantif l in Quantifier(Existe,x, (aux proba2 (x::l) (h+1)) )
			| _ -> let x = gen_quantif l in Quantifier(PourTout,x, (aux proba2 (x::l) (h+1)) )
	)
	in aux 2.0 [] 1
;;

(*(*génère une formule de la forme Existe(x)F avec F sans quantificateur*)
let rec gen_rand_quantif () =
	let rec aux proba = 
		if(Random.float(1.0) > proba)then 
		(match Random.int 3 with
			|0-> Atomique(Inf, gen_terme (Var "x") 10, gen_terme (Var "x") 10)
			|1-> Atomique(Egal, gen_terme (Var "x") 10, gen_terme (Var "x") 10)
			|_ -> Atomique(Inf, Int(Random.int 10), gen_terme (Var "x") 10)
		)
		else (
			let x = Random.int(3) in
			let proba2 = Random.float(proba) in
			match x with
				| 0 -> Not (aux proba2)
				| 1 -> Relation(Ou,(aux proba2), (aux proba2))
				| _ -> Relation(Et,(aux proba2), (aux proba2))
		)
	in
	Quantifier(Existe,"x",aux 1.0)
;;
*)

let defaultForm = [| 
Quantifier(Existe, "x", Not(Atomique(Egal,Int 10,Var"x"))) ;
Quantifier(PourTout, "x", Quantifier(Existe, "y", Atomique(Inf,Var"x",Var"y")));
Quantifier(Existe, "x", Atomique(Egal, Var"x", Produit(2,Var"x") ) );
Quantifier(PourTout, "x", Quantifier(Existe, "y", Atomique(Egal,Var"x",Produit(2,Var"y"))) );
Relation(Et, Quantifier(Existe,"x",Atomique(Egal, Int 1, Produit(7,Var "x")) ), Quantifier(Existe, "x", Not(Atomique(Egal, Produit(10,Var"x"), Plus(Var"x", Int 12))) ))
|];;



let rec readInt ()= try (let a = read_int() in if a<=0 then failwith("negative number") else a )with |_->readInt() ;;

let specifyVal = ref false;;

let printMenu () =
	for i=0 to 80 do printf("-"); done;
	printf("\n| Taimme Minimale | Taille Maximale | Afficher les Steps | Préciser les valeurs |\n");
	for i=0 to 80 do printf("-"); done;
	printf("\n|        %d        |        %d        |          %s       |          %s         |\n") (!minFormSize) (!maxFormSize) (if !printSteps then "Oui" else "Non") (if !specifyVal then "Oui" else "Non");
	for i=0 to 80 do printf("-"); done;
	printf("\nm/M pour changer la taille des formules; s pour changer l'affichege des step; v pour changer l'affichage des valeurs\nentrée pour générer une formule; q pour quiter
f1,f2,f3,f4,f5 pour charger des formules préfaites\n");
;;

let resolve f = 
	printf ("%s\n\n------------------\n\n") (formule_to_string2 f);flush stdout;

	let s2 = symplifyFormule (elimQuantif f) in
	let formuleToPrint = if !specifyVal then specifyValues s2 else formule_to_string2 s2 in
	printf ("%s\n%s\n\n\n") formuleToPrint (if valueForm s2 then "Vrai" else "Faux")
;;

let _ = 
	Random.self_init();
	while(true)do
		printMenu();
		match read_line() with 
		| "q" -> exit 1
		| "m" -> (minFormSize := readInt(); if ((!minFormSize) > (!maxFormSize)) then maxFormSize := (!minFormSize);)
		| "M" -> (maxFormSize := readInt(); if ((!minFormSize) > (!maxFormSize)) then minFormSize := (!maxFormSize);)
		| "s" -> printSteps := (not !printSteps);
		| "v" -> specifyVal := (not !specifyVal);
		| "f1" -> resolve (defaultForm.(0))
		| "f2" -> resolve (defaultForm.(1))
		| "f3" -> resolve (defaultForm.(2))
		| "f4" -> resolve (defaultForm.(3))
		| "f5" -> resolve (defaultForm.(4))
		| _ -> (
			print_endline("génération de la formule, veuillez patienter ...");
			let f = symplifyFormule (gen_rand_form ()) in
			resolve f
		)
	done;
;;
